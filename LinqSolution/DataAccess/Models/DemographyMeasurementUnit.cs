﻿namespace DataAccess.Models
{
    public class DemographyMeasurementUnit
    {
        public string SplitUnit { get; set; }
        public int ChildrenNumber { get; set; }
        public int AdultsNumber { get; set; }
        public int AverageAgeNumber { get; set; }
        public int ElderlyNumber { get; set; }
    }
}