﻿namespace DataAccess.Models
{
    public class WifiUsage
    {
        public string Id { get; set; }
        public string Localization { get; set; }
        public int UsersNumber { get; set; }

        public int TransferIncome { get; set; }
        public int TransferOutcome { get; set; }
    }
}